function resetInput(){

    $('.form_input.view2 input').on('blur change input', function(){

        var $this = $(this),
          value = $this.val();


        if(value == ''){
            $this.closest('.form_input.view2').removeClass('show');
        }
        else{
            $this.closest('.form_input.view2').addClass('show');
        }

    });

    $('.form_input.view2 .js-reset-btn').on('click', function(event) {
        $(this).closest('.form_input.view2').removeClass('show').find('input').val('');
    });
}
resetInput();

function num2str(n, text_forms) {  
    n = Math.abs(n) % 100; var n1 = n % 10;
    if (n > 10 && n < 20) { return text_forms[2]; }
    if (n1 > 1 && n1 < 5) { return text_forms[1]; }
    if (n1 == 1) { return text_forms[0]; }
    return text_forms[2];
}

function calc(){
    var tr = $('.filter_order_table tbody tr'),
        sum = 0,
        count = 0,
        summ,
        countt;

    tr.each(function(){
        if(!$(this).find('input:checkbox').prop('checked')){return}
        var checkbox = $(this).find('input:checkbox').prop('checked'),
            price = $(this).find('td.table_price span').text().replace(/\s/g, '');
        sum += parseFloat(price);
        count += 1;
    });

    summ = sum.toFixed(2) + '';
    countt = count + '';
    // .replace(/[,]+/g, '.') -- for del , and add .

    $('body .filter_order_price_it').text(summ.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
    $('body .filter_order_choose_it').text(countt.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));

    var val = num2str(count, ['слеб', 'слеба', 'слебов']),
        vals = num2str(count, ['Выбран', 'Выбрано', 'Выбрано']);
    $('body .filter_order_choose_txt').text(val);
    $('body .filter_order_choose_v').text(vals);

    CheckTb();
}
calc()

function tableCheck() {
    $('.filter_order_table .main_check').on('click change input', function(event) {
        
        if($(this).prop('checked')){
            $(this).closest('.filter_order_table').find('tbody tr td input:checkbox').prop('checked', true).trigger('refresh');
        }
        else{
            $(this).closest('.filter_order_table').find('tbody tr td input:checkbox').prop("checked", false).trigger('refresh');
        }
        calc();
    });

    $('.filter_order_table').find('tbody tr td:first-child input').on('click change input', function(event){
        calc();
    });
    
}
tableCheck();

function popupAddBask(){
    $('body').on('click', '.js-add-basket', function(){
        var $checkbox = $("#"+$(this).attr('data-checked-id'));
        
        if($(this).hasClass('check')){
            $(this).removeClass('check');
            $checkbox.prop('checked', false).trigger('refresh');
        }
        else{
            $(this).addClass('check');
            $checkbox.prop('checked', true).trigger('refresh');
        }
        calc();
    });
}
popupAddBask();

// filter start

function filterCheck() {
	$('body .filmain_check').on('click change input', function(event) {
		$(this).toggleClass('checked');
		if($(this).hasClass('checked')){
			$(this).closest('.form_checkbox').find('input:checkbox').addClass('checked').prop('checked', true).trigger('refresh');
			$(this).closest('.form_row').addClass('show');
		}
		else{
            var $cheID = $(this).closest('.form_checkbox').attr('id');

			$(this).closest('.form_checkbox').find('input:checkbox').removeClass('checked').prop('checked', false).trigger('refresh');
			$(this).closest('.form_row').removeClass('show');
            $('body').find('[data-id='+ $cheID +']').find('.sort_data_box').trigger('click');
		}
		filterCheckLength($(this), $(this).closest('.form_checkbox').find("input:checkbox:checked").length  - 1);
        
        var $inpVal = $(this).closest('.form_checkbox').find('input:checkbox:checked:not(.filmain_check)');
        
        $inpVal.each(function(){
          var $Id = $(this).prop('id');
            filterTemplate($(this), $Id);
        });
	});

	$('body .form_row .form_checkbox input:checkbox:not(.filmain_check)').on('click change input', function(event) {
        var $This = $(this),
            $Id = $This.prop('id');

		if(!$(this).hasClass('checked')){
			$(this).toggleClass('checked').prop('checked', true).trigger('refresh');
			$(this).closest('.form_row').addClass('show');
		}
		else{
			$(this).removeClass('checked').prop('checked', false).trigger('refresh');
			
			if($(this).closest('.form_checkbox').find('input:checkbox:checked').length <= 0){
				$(this).closest('.form_row').removeClass('show');
			}
			if($(this).not(':checked') && $(this).closest('.form_checkbox').find('.filmain_check').hasClass('checked')){
				$(this).closest('.form_checkbox').find('.filmain_check').removeClass('checked').prop('checked', false).trigger('refresh');
				filterCheckLength($(this), $(this).closest('.form_checkbox').find("input:checkbox:checked").length );
			}

		}
		filterCheckLength($(this), $(this).closest('.form_checkbox').find("input:checkbox:checked").length );
        filterTemplate($This, $Id);
	});
}
filterCheck();

function filterCheckLength(thats, check) {
	var $allCheck = check;
	thats.closest('.form_row').find('.form_title .form_numb').text($allCheck);
}

function filterCheckReset() {
    $('.filter_form .js-reset-btn').on('click', function(event) {
        var $dID = $(this).closest('.form_row').find('.form_checkbox').attr('id');

        $(this).closest('.form_row').removeClass('show').find('.form_checkbox input:checkbox').removeClass('checked').prop('checked', false).trigger('refresh');

        $('body').find('[data-id='+ $dID +']').find('.sort_data_box').trigger('click');
    });
}
filterCheckReset();

function filterFormReset() {
	$('.filter_form button[type="reset"]').on('click', function(event) {
		$('.filter_form').find('input:checkbox').removeClass('checked').prop('checked', false).trigger('refresh');
		$('.filter_form').find('.form_row').removeClass('show');
		$('.filter_form').find('.form_input.view2').removeClass('show');
		$('.form_numb').text('');
        $(this).closest('form').get(0).reset();
		$('.filter_form').find('select').trigger('refresh');  
        $('body').find('.sort_data_item .sort_data_box').trigger('click');
    });
}
filterFormReset();

function delSort(){
    $('body').on('click', '.sort_data_box', function(event) {
        var $boxID = $(this).attr('data-id');
        
        $('body').find('[id='+ $boxID +']').removeClass('checked').prop('checked', false).trigger('refresh');
        $('body').find('[id='+ $boxID +']').closest('.form_checkbox').find('.filmain_check').removeClass('checked').prop('checked', false).trigger('refresh');
        $(this).remove();
        filterCheckLength($('body').find('[id='+ $boxID +']').closest('.form_checkbox'), $('body').find('[id='+ $boxID +']').closest('.form_checkbox').find("input:checkbox:checked:not(.filmain_check)").length);
        checkSort();
    });
}
delSort();

function checkSort(){
    $('body .sort_data_item').each(function(){
        if(!$(this).find('.sort_data_box').length){
            var $dataID = $(this).attr('data-id');
            
            $('body').find('[id='+ $dataID +']').removeClass('active').closest('.form_row').removeClass('show');
            $(this).remove();
        } 
    });
}

function filterTemplate(that, checkboxId){
    var $box = $('.sort_data'),
        $Name = $(that).attr('data-name'),
        $CheckId = $(that).closest('.form_checkbox').attr('id'),
        $Label = $(that).closest('.form_checkbox').attr('data-name'),
        $Template1 =    '<div data-id="'+ $CheckId +'" class="sort_data_item">'+
                            '<div class="sort_data_label"><span>'+ $Label +'</span>:</div>'+
                            '<div class="sort_data_main">'+
                                '<div class="sort_data_box" data-id="'+ checkboxId +'"><span>'+ $Name +'</span></div>'+
                            '</div>'+
                        '</div>',
        $Template2 = '<div class="sort_data_box" data-id="'+ checkboxId +'"><span>'+ $Name +'</span></div>';

        if(!$(that).closest('.form_checkbox').hasClass('active')){
            $(that).closest('.form_checkbox').addClass('active')
            $('body').find($box).append($Template1);
        }
        else{
            if(that.prop('checked') == false){
                $('body').find($box).find('[data-id='+ checkboxId +']').remove();
                checkSort()
            }
            else{
                $('body').find($box).find('[data-id='+ $CheckId +']').find('.sort_data_main').append($Template2);
            }
        }
}
// filter end

// closeTr start
function closeTr(){
    $('.js-close-tr').on('click', function(event) {
        event.preventDefault();
        $(this).closest('tr').addClass('hideTr').animate({opacity: 0},500, function() {
            $(this).remove();
            CheckTb();
            calc();
        });
        setTimeout(function(){
            $('.filter_order_table.basket_table').each(function(){
                // console.log($(this).find('tbody:empty').length < 1)
                if($(this).find('tbody tr').length < 1){
                    if(!$(this).hasClass('tableEmpty')){
                        $(this).hide().addClass('tableEmpty').closest('.filter_table_wrap').append('<div class="emptyText">Таблица пуста.</div>')
                    }
                }
            })
        },500) 
    });
}closeTr();
// closeTr end

function CheckTb(){
    $('.filter_order_table.basket_table').each(function(){
        if($(this).find('tbody tr').length < 1){
            if(!$(this).hasClass('tableEmpty')){
                $(this).hide().addClass('tableEmpty').closest('.filter_table_wrap').append('<div class="emptyText">Таблица пуста.</div>')
                $(this).closest('.basket_wr_item').addClass('wrEmpty').hide().remove();  
            }
        }
    })
    setTimeout(function(){
        if($.trim($('body .basket_wrap').html()) !== '' ){
            // console.log(22)
        }
        else{
            $('body .basket_wrap').append('<div class="emptyText">Корзина пуста.</div>')
            // console.log(33)
        }
        
    },1000)
}