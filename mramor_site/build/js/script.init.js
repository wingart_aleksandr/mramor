// var $base_url= document.write(location.protocol,'//',location.host);
// console.log($base_url)
$(document).ready(function(){


	/* ------------------------------------------------
	TouchSwipe START
	------------------------------------------------ */
			// if($("body").length){

				// $("body").swipe({
				// 	tap:function(event, target) {
			          
			 //        },
				// 	swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
				// 		if(direction === 'left'){
				// 			$('body .mfp-gallery .mfp-arrow-right').trigger('click');
				// 		}
				// 		if(direction === 'right'){
				// 			$('body .mfp-gallery .mfp-arrow-left').trigger('click');
				// 		}
				// 	},
				// });

			// }

	/* ------------------------------------------------
	TouchSwipe END
	------------------------------------------------ */



	/* ------------------------------------------------
	Clipboard START
	------------------------------------------------ */

			function clipBoard(){
        		if($('body .copy-link').length){
        			// console.log(1)
        			new ClipboardJS('body .copy-link');
        			$('body .copy-link').on('click tap', function(e){
        				if($(this).closest('.mfp-container').length){
        					e.preventDefault();
        					$(this).addClass('disabled');
        				}
        			})
        		}
    		}
    		clipBoard();

	/* ------------------------------------------------
	Clipboard END
	------------------------------------------------ */



	/* ------------------------------------------------
	Validate START
	------------------------------------------------ */

			if($('.js-validate').length){

				$('.js-validate').validate({

					rules:{

						// val_name: {
						// 	required: true,
						// 	minlength: 2
						// },

						val_email: {
							required: true,
							email: true
						},

						val_tel: {
							required: true,
							minlength: 10
						},

					},

					messages:{

						// val_name: {
						// 	required: "Обязательно сообщите имя",
						// 	minlength: 'Введите не менее 2 символов.'
						// },

						val_email: {
							required: "Обязательно сообщите адрес",
							email: "Не верный email."
						},

						val_tel: {
							required: "Обязательно сообщите номер",
							minlength: "Введите не менее 10 символов."
						},

					}

				});	

			}

			if($('.js-validate2').length){

				$('.js-validate2').validate({

					rules:{

						// val_name: {
						// 	required: true,
						// 	minlength: 2
						// },

						// val_comment: {
						// 	required: true
						// },

						val_tel: {
							required: true,
							minlength: 10
						},

					},

					messages:{

						// val_name: {
						// 	required: "Обязательно сообщите имя",
						// 	minlength: 'Введите не менее 2 символов.'
						// },

						// val_comment: {
						// 	required: "Обязательно напишите комментарий"
						// },

						val_tel: {
							required: "Обязательно сообщите номер",
							minlength: "Введите не менее 10 символов."
						},

					},

					submitHandler: function(form) {
						if($(form).valid() == true){
							form.submit();
							if ($('#modal_order_processed').length) {
							    $.magnificPopup.open({
							        items: {
							            src: '#modal_order_processed' 
							        },
							        type: 'inline'
							    });
							}
						}
					}

				});	

			}

			if($('.js-validate3').length){

				$('.js-validate3').validate({

					rules:{

						// val_name: {
						// 	required: true,
						// 	minlength: 2
						// },

						// val_comment: {
						// 	required: true
						// },

						val_tel: {
							required: true,
							minlength: 10
						},

					},

					messages:{

						// val_name: {
						// 	required: "Обязательно сообщите имя",
						// 	minlength: 'Введите не менее 2 символов.'
						// },

						// val_comment: {
						// 	required: "Обязательно напишите комментарий"
						// },

						val_tel: {
							required: "Обязательно сообщите номер",
							minlength: "Введите не менее 10 символов."
						},

					},
					
					submitHandler: function(form) {
						if($(form).valid() == true){
							form.submit();
							if ($('#modal_thanks').length) {
							    $.magnificPopup.open({
							        items: {
							            src: '#modal_thanks' 
							        },
							        type: 'inline'
							    });
							}
						}
					}

				});	


			}

			if($('.js-validate4').length){

				$('.js-validate4').validate({

					rules:{

						// val_name: {
						// 	required: true,
						// 	minlength: 2
						// },

						val_email: {
							required: true,
							email: true
						},

						val_tel: {
							required: true
						},

					},

					messages:{

						// val_name: {
						// 	required: "Обязательно сообщите имя",
						// 	minlength: 'Введите не менее 2 символов.'
						// },

						val_email: {
							required: "Обязательно сообщите адрес",
							email: "Не верный email."
						},

						val_tel: {
							required: "Обязательно сообщите номер",
						},

					}

				});	


			}

	/* ------------------------------------------------
	Validate END
	------------------------------------------------ */



	/* ------------------------------------------------
	Autocomplete START
	------------------------------------------------ */

			if ($(".autocomplete").length){

				var options = {
				  url: "data/search.json",
				  getValue: "name",
				  requestDelay: 300,
				  minCharNumber: 3,

				  list: {	
				    showAnimation: {
		    			type: "fade", //normal|slide|fade
		    			time: 400,
		    			callback: function() {}
		    		},

		    		hideAnimation: {
		    			type: "fade", //normal|slide|fade
		    			time: 400,
		    			callback: function() {}
		    		},

				    match: {
				      enabled: true
				    }
				  }
				};

				$(".autocomplete").easyAutocomplete(options);

				$(".autocomplete").on('focus tap', function(event) {
					$(this).closest('.easy-autocomplete').next('.btn_search').addClass('focus');
				});
				
				$(".autocomplete").on('blur tap', function(event) {
					$(this).closest('.easy-autocomplete').next('button.btn_search').removeClass('focus');
				});

			}

	/* ------------------------------------------------
	Autocomplete END
	------------------------------------------------ */



	/* ------------------------------------------------
	Inputmask START
	------------------------------------------------ */

			if($('.mask, input[type="tel"]').length){

				$('.mask, input[type="tel"]').inputmask({
					"mask": "+7 (999) 999-9999",
					'showMaskOnHover': false,
					"clearIncomplete": true,
					'oncomplete': function(){ 
						// console.log('Ввод завершен'); 
					}, 
					'onincomplete': function(){ 
						// console.log('Заполнено не до конца'); 
					},
					'oncleared': function(){ 
						// console.log('Очищено'); 
					}
				});

			}

	/* ------------------------------------------------
	Inputmask END
	------------------------------------------------ */




	/* ------------------------------------------------
	MagnificPopup START
	------------------------------------------------ */

			if ($('.filter_order_table').length){

				$('.filter_order_table').magnificPopup({
					delegate: '.table_img',
					type: 'image',
					fixedContentPos: true,
					// tLoading: 'Loading image #%curr%...',
					// mainClass: 'mfp-img-mobile',
					mainClass: 'mfp-zoom-in',
	        		removalDelay: 300, //delay removal by X to allow out-animation
					closeOnBgClick: true,
	    			closeBtnInside: true,

		    		gallery: {
						enabled: true, 
						navigateByImgClick: false,
						// titleSrc: 'title',
						tCounter: '<span>%curr% / %total%</span>'
		    		},
		    		
		    		image: {
				        titleSrc: function(item) {

				        	if(item.el.attr('data-old-price') && !(item.el.attr('data-reserve'))){
								var $slebName = item.el.parents('td').find('.table_img').data('name'),
					        		$slebArticul = item.el.parents('td').find('.table_img').data('articul'),
					        		$slebMetr = item.el.parents('td').find('.table_img').data('metr'),
					        		$slebPhotoSrc = item.el.parents('td').find('.table_img').data('photosrc'),
					        		$slebCopySrc = item.el.parents('td').find('.table_img').data('copy-src'),
					        		$slebPrice = item.el.parents('td').find('.table_img').data('price'),
					        		$slebPrice2 = item.el.parents('td').find('.table_img').data('price2'),
						        		$slebOldPrice = item.el.parents('td').find('.table_img').data('old-price'),
						        		$slebPercent = item.el.parents('td').find('.table_img').data('percent'),
						        		$slebPercentTime = item.el.parents('td').find('.table_img').data('percent-time'),
						        	$slebProportion = item.el.parents('td').find('.table_img').data('proportion'),
					        		$slebCheckedId = item.el.parents('td').find('.table_img').data('checked-id'),
					        		$template = "<div class='popup_sleb_descr'>"+
					        						"<span class='popup_sleb_name'>"+ $slebName +"</span>"+
					        						"<span class='popup_sleb_articul'>"+ $slebArticul +"</span>"+
					        						"<span class='popup_sleb_proportion'>"+ $slebProportion +"</span>"+
					        						"<span class='popup_sleb_metr'><span>"+ $slebMetr +"</span> м<sup>2</sup></span>"+
					        					"</div>"+
					        					"<div class='popup_sleb_photo'>"+
					        						"<a class='btn-radius btn-l btn-bd-white' href='"+ $slebPhotoSrc +"'>Скачать оригинал фото</a>"+
					        						"<a class='copy-link btn-radius btn-l btn-bd-white' href='javascript:;' data-clipboard-target='#bar' data-text='Скопировать ссылку на слеб' data-text2='Ссылка скопирована'><textarea id='bar'>"+ $slebCopySrc +"</textarea></a>"+
					        					"</div>"+
					        					
					        					"<div class='popup_sleb_price2'>"+$slebPrice2+" ₽/м<sup>2</sup></div>"+
					        					"<div class='popup_sleb_price-btn'>"+
					        						"<div class='popup_sleb_discount'>"+
					        							"<span class='old_price'>"+ $slebOldPrice +"₽</span>"+
						        						"<span class='discount_flag'>"+
						        						    "<span>-<span>"+ $slebPercent +"</span>%</span>"+
						        						    "<span class='discount_time'>до <time>"+ $slebPercentTime +"</time>"+
						        						    "</span>"+
						        						"</span>"+
						        					"</div>"+
					        						"<div class='popup_sleb_price'>"+
						        						"<span>"+ $slebPrice +"</span> ₽"+
						        					"</div>"+
						        					"<a class='btn-radius btn-l btn-orange js-add-basket' href='javascript:;' data-checked-id='"+ $slebCheckedId +"' data-def-text='Добавить в корзину' data-text='Убрать из корзины'></a>"+
					        					"</div>";
					        					
				        	}
				        	else if(item.el.attr('data-old-price') && item.el.attr('data-reserve')){
				        		var $slebName = item.el.parents('td').find('.table_img').data('name'),
					        		$slebArticul = item.el.parents('td').find('.table_img').data('articul'),
					        		$slebMetr = item.el.parents('td').find('.table_img').data('metr'),
					        		$slebPhotoSrc = item.el.parents('td').find('.table_img').data('photosrc'),
					        		$slebCopySrc = item.el.parents('td').find('.table_img').data('copy-src'),
					        		$slebPrice = item.el.parents('td').find('.table_img').data('price'),
					        		$slebPrice2 = item.el.parents('td').find('.table_img').data('price2'),
						        		$slebOldPrice = item.el.parents('td').find('.table_img').data('old-price'),
						        		$slebPercent = item.el.parents('td').find('.table_img').data('percent'),
						        		$slebPercentTime = item.el.parents('td').find('.table_img').data('percent-time'),
						        	$slebProportion = item.el.parents('td').find('.table_img').data('proportion'),
					        		$slebCheckedId = item.el.parents('td').find('.table_img').data('checked-id'),
					        		$template = "<div class='popup_sleb_descr'>"+
					        						"<span class='popup_sleb_name'>"+ $slebName +"</span>"+
					        						"<span class='popup_sleb_articul'>"+ $slebArticul +"</span>"+
					        						"<span class='popup_sleb_proportion'>"+ $slebProportion +"</span>"+
					        						"<span class='popup_sleb_metr'><span>"+ $slebMetr +"</span> м<sup>2</sup></span>"+
					        					"</div>"+
					        					"<div class='popup_sleb_photo'>"+
					        						"<a class='btn-radius btn-l btn-bd-white' href='"+ $slebPhotoSrc +"'>Скачать оригинал фото</a>"+
					        						"<a class='copy-link btn-radius btn-l btn-bd-white' href='javascript:;' data-clipboard-target='#bar' data-text='Скопировать ссылку на слеб' data-text2='Ссылка скопирована'><textarea id='bar'>"+ $slebCopySrc +"</textarea></a>"+
					        					"</div>"+
					        					
					        					"<div class='popup_sleb_price2'>"+$slebPrice2+" ₽/м<sup>2</sup></div>"+
					        					"<div class='popup_sleb_price-btn'>"+
					        						"<div class='popup_sleb_discount'>"+
					        							"<span class='old_price'>"+ $slebOldPrice +"₽</span>"+
						        						"<span class='discount_flag'>"+
						        						    "<span>-<span>"+ $slebPercent +"</span>%</span>"+
						        						    "<span class='discount_time'>до <time>"+ $slebPercentTime +"</time>"+
						        						    "</span>"+
						        						"</span>"+
						        					"</div>"+
					        						"<div class='popup_sleb_price'>"+
						        						"<span>"+ $slebPrice +"</span> ₽"+
						        					"</div>"+
						        					"<div class='reserve_label'><span>Резерв</span></div>"+
					        					"</div>";
					        					
				        	}
				        	else if(item.el.attr('data-reserve')){
				        		var $slebName = item.el.parents('td').find('.table_img').data('name'),
					        		$slebArticul = item.el.parents('td').find('.table_img').data('articul'),
					        		$slebMetr = item.el.parents('td').find('.table_img').data('metr'),
					        		$slebPhotoSrc = item.el.parents('td').find('.table_img').data('photosrc'),
					        		$slebCopySrc = item.el.parents('td').find('.table_img').data('copy-src'),
					        		$slebPrice = item.el.parents('td').find('.table_img').data('price'),
					        		$slebPrice2 = item.el.parents('td').find('.table_img').data('price2'),
					        		$slebProportion = item.el.parents('td').find('.table_img').data('proportion'),
					        		$slebCheckedId = item.el.parents('td').find('.table_img').data('checked-id'),
					        		$template = "<div class='popup_sleb_descr'>"+
					        						"<span class='popup_sleb_name'>"+ $slebName +"</span>"+
					        						"<span class='popup_sleb_articul'>"+ $slebArticul +"</span>"+
					        						"<span class='popup_sleb_proportion'>"+ $slebProportion +"</span>"+
					        						"<span class='popup_sleb_metr'><span>"+ $slebMetr +"</span> м<sup>2</sup></span>"+
					        					"</div>"+
					        					"<div class='popup_sleb_photo'>"+
					        						"<a class='btn-radius btn-l btn-bd-white' href='"+ $slebPhotoSrc +"'>Скачать оригинал фото</a>"+
					        						"<a class='copy-link btn-radius btn-l btn-bd-white' href='javascript:;' data-clipboard-target='#bar' data-text='Скопировать ссылку на слеб' data-text2='Ссылка скопирована'><textarea id='bar'>"+ $slebCopySrc +"</textarea></a>"+
					        					"</div>"+
					        					"<div class='popup_sleb_price2'>"+$slebPrice2+" ₽/м<sup>2</sup></div>"+
					        					"<div class='popup_sleb_price-btn'>"+
					        						"<div class='popup_sleb_price'>"+
						        						"<span>"+ $slebPrice +"</span> ₽"+
						        					"</div>"+
						        					"<div class='reserve_label'><span>Резерв</span></div>"+
					        					"</div>";
				        	}
				        	else{
				        		var $slebName = item.el.parents('td').find('.table_img').data('name'),
					        		$slebArticul = item.el.parents('td').find('.table_img').data('articul'),
					        		$slebMetr = item.el.parents('td').find('.table_img').data('metr'),
					        		$slebPhotoSrc = item.el.parents('td').find('.table_img').data('photosrc'),
					        		$slebCopySrc = item.el.parents('td').find('.table_img').data('copy-src'),
					        		$slebPrice = item.el.parents('td').find('.table_img').data('price'),
					        		$slebPrice2 = item.el.parents('td').find('.table_img').data('price2'),
					        		$slebProportion = item.el.parents('td').find('.table_img').data('proportion'),
					        		$slebCheckedId = item.el.parents('td').find('.table_img').data('checked-id'),
					        		$template = "<div class='popup_sleb_descr'>"+
					        						"<span class='popup_sleb_name'>"+ $slebName +"</span>"+
					        						"<span class='popup_sleb_articul'>"+ $slebArticul +"</span>"+
					        						"<span class='popup_sleb_proportion'>"+ $slebProportion +"</span>"+
					        						"<span class='popup_sleb_metr'><span>"+ $slebMetr +"</span> м<sup>2</sup></span>"+
					        					"</div>"+
					        					"<div class='popup_sleb_photo'>"+
					        						"<a class='btn-radius btn-l btn-bd-white' href='"+ $slebPhotoSrc +"'>Скачать оригинал фото</a>"+
					        						"<a class='copy-link btn-radius btn-l btn-bd-white' href='javascript:;' data-clipboard-target='#bar' data-text='Скопировать ссылку на слеб' data-text2='Ссылка скопирована'><textarea id='bar'>"+ $slebCopySrc +"</textarea></a>"+
					        					"</div>"+
					        					"<div class='popup_sleb_price2'>"+$slebPrice2+" ₽/м<sup>2</sup></div>"+
					        					"<div class='popup_sleb_price-btn'>"+
					        						"<div class='popup_sleb_price'>"+
						        						"<span>"+ $slebPrice +"</span> ₽"+
						        					"</div>"+
						        					"<a class='btn-radius btn-l btn-orange js-add-basket' href='javascript:;' data-checked-id='"+ $slebCheckedId +"' data-def-text='Добавить в корзину' data-text='Убрать из корзины'></a>"+
					        					"</div>";
				        	}
					        
					        return $template;

				        }
				    },

		    		callbacks: {
		    		    open: function(item) {
				    		var img = $(".mfp-figure>figure").find('img');
				    		img.parent().prepend('<div class="mfp_img_box"></div>');
				    		img.parent().find('.mfp_img_box').prepend(img);

				    		//overwrite default prev + next function. Add timeout for css3 crossfade animation
			                $.magnificPopup.instance.next = function() {
			                    var self = this;
			                    self.wrap.removeClass('mfp-image-loaded');
			                    setTimeout(function() { $.magnificPopup.proto.next.call(self); }, 120);
			                }
			                $.magnificPopup.instance.prev = function() {
			                    var self = this;
			                    self.wrap.removeClass('mfp-image-loaded');
			                    setTimeout(function() { $.magnificPopup.proto.prev.call(self); }, 120);
			                }

			                clipBoard();

		    		    },
		    		    imageLoadComplete: function() { 
			                var self = this;
			                setTimeout(function() { self.wrap.addClass('mfp-image-loaded'); }, 16);
			            },
			            change: function() {
						    popupAddbasket();
						    clipBoard();
						},
		    		    elementParse: function(item) {
		    		        $('.mfp-counter').prependTo(".mfp-figure>figure .mfp_img_box");

		    		        popupAddbasket();
		    		        clipBoard();
		    		    },
		    		    // buildControls: function() {
	    		     //      this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
	    		     //    }

		    		}
				});

				function indexTr(){
				    $('.filter_order_table tbody tr').each(function(index, el){
				        $(el).attr("data-index", index);
				    })              
				}

				function tableOpenPopup() {
				    if($(window).width() >= 992){

				        indexTr();

				        $('.filter_order_table tbody tr').on("click touchstart tap", function(event) {
				        	if(!$(this).closest('.filter_order_table.basket_table').length){
					            if($(event.target).closest("td:first-child").length){return}
					            var index = $(this).data('index');
					        	console.log($(event.target))

					            $('.filter_order_table').magnificPopup('open', index);
				        	}
				        });
				    }
				}
				tableOpenPopup();

		        function popupAddbasket(){
		            setTimeout(function(){
		            	var $checkbox = $("#"+$('body').find('.js-add-basket').attr('data-checked-id'));

			            if($('body').find($checkbox).prop('checked')){
			            	$('body').find('.js-add-basket').addClass('check');
			            }
			            else{
			            	$('body').find('.js-add-basket').removeClass('check');
			            }
		            }, 500);
		        }

	        }

	        if ($('.js-modal').length){
	        	$('.js-modal').magnificPopup({
					type: 'inline',
					mainClass: 'mfp-zoom-in',
	        		removalDelay: 500,
					closeOnBgClick: true,
	    			closeBtnInside: true,
	    			callbacks: {
						open: function() {
							$('body').addClass('modalFormOpen');
						},
						close: function() {
							$('body').removeClass('modalFormOpen');
						}
	    			}
	        	});
	        }

	/* ------------------------------------------------
	MagnificPopup END
	------------------------------------------------ */




	/* ------------------------------------------------
	FORMSTYLER START
	------------------------------------------------ */

			if ($('input[type="number"],input[type="checkbox"],input[type="radio"],select').length){
				$('input[type="number"],input[type="checkbox"],input[type="radio"],select').styler({
					selectSmartPositioning: true
				});
			}

	/* ------------------------------------------------
	FORMSTYLER END
	------------------------------------------------ */




	/* ------------------------------------------------
	Owl START
	------------------------------------------------ */

			if($('.js-carousel').length){
				var $carousel = $('.js-carousel');
				$carousel.owlCarousel({
				    loop:true,
				    items: 1,
				    nav:false
				});
				$('.tile-prev').on('click tap', function(){
					$(this).closest('.tile_slider').find($carousel).trigger('prev.owl.carousel', [300]);
				})
				$('.tile-next').on('click tap', function(){
					$(this).closest('.tile_slider').find($carousel).trigger('next.owl.carousel', [300]);
				})
			}

	/* ------------------------------------------------
	Owl END
	------------------------------------------------ */


});

$(window).load(function() {});