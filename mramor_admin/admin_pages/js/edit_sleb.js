
// калькуляция суммы одного слеба
function sumSleb(sleb) {
	
	var qtSleb = sleb.find('.qt_sleb').text(),
		priceSleb = sleb.find('.price_sleb').text().replace(/\s+/g, ''),
		sumSleb = sleb.find('.sum_sleb'),
		priceVal = (qtSleb * priceSleb).toFixed(2) + '',
		outSum = priceVal.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');

	sumSleb.text(outSum);


}

// калькуляция суммы слебов в одном подблоке
function sumSlebItem(slebItem) {
	
	var sumSleb = slebItem.find('.sum_sleb'),
		sumSlebItem = slebItem.find('.sum_item'),
		outSum,
		sum = 0;

	for (var i = sumSleb.length - 1; i >= 0; i--) {
		var sumSlebVal = $(sumSleb[i]).text().replace(/\s+/g, ''),
		sum = +sum + +sumSlebVal;
		sum = sum.toFixed(2);
	}
	sum = sum + '';
	outSum = sum.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');

	sumSlebItem.text(outSum);
}

// калькуляция общей суммызаказа
function totalAmount(){

	var sumSlebItem = $('.sleb_edit_wrapp').find('.sum_item'),
		totalAmount = $('.sleb_edit_wrapp').find('.total_amount'),
		outSum,
		sum = 0;

	for (var i = sumSlebItem.length - 1; i >= 0; i--) {
		var sumSlebVal = $(sumSlebItem[i]).text().replace(/\s+/g, ''),
		sum = +sum + +sumSlebVal;
		sum = sum.toFixed(2);
	}
	sum = sum + '';
	outSum = sum.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');

	totalAmount.text(outSum);
}

// Добавить слеб в подблок
function addSleb(idSleb, dataSleb, select, row){

	var template = '<tr data-id="'+idSleb+'">\
			<td>'+dataSleb.art+'</td>\
			<td>'+dataSleb.comment+'</td>\
			<td class="">'+dataSleb.size+'</td>\
			<td class="qt_sleb">'+dataSleb.square+'</td>\
			<td class="price_sleb">'+dataSleb.price+'</td>\
			<td class="sum_sleb"></td>\
			<td class="text-center">\
				<button class="btn btn-danger sleb_del">\
					<i class="font-icon font-icon-trash"></i>\
				</button>\
			</td>\
		</tr>';

	row.before(template);

	select.val(null)
		  .trigger('change')
		  .find('option[data-id*="'+idSleb+'"]')
		  .prop('disabled', true);

	select.select2({
		minimumResultsForSearch: "Infinity",
		theme: "arrow"
	});

}

// Добавить подблок
function addSlebItem(dataItem, idSleb, dataSleb){

	var template1 = '<div class="sleb_edit_item mb-5" data-nameId="'+dataItem.nameId+'" data-blockId="'+dataItem.blockId+'" data-subBlockId="'+dataItem.subBlockId+'">\
						<h5 class="with-border">\
							'+dataItem.name+' '+dataItem.block+' '+dataItem.subBlock+' \
							<button type="button" class="btn btn-danger sleb_item_del">\
								<i class="font-icon font-icon-trash"></i>\
							</button>\
							</br> \
							<small class="text-muted smaller">Поставщик: ИП Никифоров</small>\
						</h5>\
						<table class="table table-bordered table-hover">\
							<thead><tr><th>Арт.</th><th>Комментарий</th><th>ДхВхШ мм</th><th>S, м2</th><th>Цена, р/м2</th><th>Стоимость, р.</th><th class="table-icon-cell"></th></tr></thead>\
							<tbody>\
								<tr>\
									<td colspan="5">\
										<label class="form-label">\
											Добавить слеб из этого подблока\
										</label>\
										<div class="m-b select_box">\
										</div>\
										<button type="submit" class="btn btn-success js_add_sleb">Добавить</button>\
									</td>\
									<td colspan="3" class="sum_item_cell">Сумма: <span class="sum_item"></span> р.</td>\
								</tr>\
							</tbody>\
						</table>\
					</div>';

	var template2 = '<tr data-id="'+idSleb+'">\
			<td>'+dataSleb.art+'</td>\
			<td>'+dataSleb.comment+'</td>\
			<td class="">'+dataSleb.size+'</td>\
			<td class="qt_sleb">'+dataSleb.square+'</td>\
			<td class="price_sleb">'+dataSleb.price+'</td>\
			<td class="sum_sleb"></td>\
			<td class="text-center">\
				<button class="btn btn-danger sleb_del">\
					<i class="font-icon font-icon-trash"></i>\
				</button>\
			</td>\
		</tr>';

	$('.sleb_add_item').before(template1);
	$('.sleb_add_item').prev('.sleb_edit_item').find('.select_box').append(dataItem.select);
	$('.sleb_add_item').prev('.sleb_edit_item').find('tbody>tr:last-child').before(template2);
	
	$('.sleb_add_item').prev('.sleb_edit_item')
					   .find('.select_box>select')
		  			   .val(null)
		  			   .find('option[data-id*="'+idSleb+'"]')
		  			   .prop('disabled', true);

	$('.sleb_add_item').prev('.sleb_edit_item').find('.select_box>select').removeAttr('id').select2({
		minimumResultsForSearch: "Infinity",
		theme: "arrow"
	});

	$('#name_sub_block').val(null)
		  .trigger('change')
		  .find('option[data-subBlockId*="'+dataItem.subBlockId+'"]')
		  .prop('disabled', true);

	$('#name_sub_block').select2({
		minimumResultsForSearch: "Infinity",
		theme: "arrow"
	});

	sumSleb($('.sleb_add_item').prev('.sleb_edit_item').find('tbody>tr:first-child'));
	sumSlebItem($('.sleb_add_item').prev('.sleb_edit_item'));
	
}

$('.sleb_edit_wrapp').find('.sum_sleb').closest('tr').each(function(){
	sumSleb($(this));

})

$('.sleb_edit_wrapp').find('.sleb_edit_item').each(function(){
	sumSlebItem($(this));
})

totalAmount();

$('.sleb_edit_wrapp').find('.qt_sleb>input').on('change keyup input click', function(){
	sumSleb($(this).closest('tr'));
	sumSlebItem($(this).closest('.sleb_edit_item'));
	totalAmount();

})

$('.sleb_edit_wrapp').on('click', '.sleb_item_del', function(){
	
	var subBlockId = $(this).closest('.sleb_edit_item').attr('data-subBlockId');

	$(this).closest('.sleb_edit_item').remove();

	$('#name_sub_block').val(null)
		  .trigger('change')
		  .find('option[data-subBlockId*="'+subBlockId+'"]')
		  .prop('disabled', false);

	$('#name_sub_block').select2({
		minimumResultsForSearch: "Infinity",
		theme: "arrow"
	});

	totalAmount();
})

$('.sleb_edit_wrapp').on('click', '.sleb_del', function(){
	var parent = $(this).closest('.sleb_edit_item'),
		idSleb = $(this).closest('tr').attr('data-id');

	$(this).closest('tr').remove();
	parent.find('.select2')
		  .find('option[data-id*="'+idSleb+'"]')
		  .prop('disabled', false);
	console.log(idSleb);
	parent.find('.select2').select2({
			minimumResultsForSearch: "Infinity",
			theme: "arrow"
		});
	sumSlebItem(parent);
	totalAmount();
})

$('.sleb_edit_wrapp').on('click', '.js_add_sleb', function(){
	var parent = $(this).closest('.sleb_edit_item'),
		select = $(this).closest('tr').find('.select2'),
		dataSleb = select.find(':selected').data('sleb'),
		idSleb = select.find(':selected').attr('data-id');

	addSleb(idSleb, dataSleb, select, parent.find('tbody>tr:last-child'))
	sumSleb($(this).closest('tr').prev());
	sumSlebItem(parent);
	totalAmount();
})


$('.sleb_edit_wrapp').on('click', '.js_add_sleb_item', function(){
	var parent = $(this).closest('.sleb_add_item'),
		select = parent.find('#name_sleb'),
		dataSleb = select.find(':selected').data('sleb'),
		idSleb = select.find(':selected').attr('data-id')
		dataItem = {
			'name':parent.find('#stone_name').val(),
			'nameId':parent.find('#stone_name').find('option:selected').attr('data-nameId'),
			'block':parent.find('#name_block').val(),
			'blockId':parent.find('#name_block').find('option:selected').attr('data-blockId'),
			'subBlock':parent.find('#name_sub_block').val(),
			'subBlockId':parent.find('#name_sub_block').find('option:selected').attr('data-subBlockId'),
			'select': select.clone()
		};

	select.find(':selected').prop('disabled', true);
	addSlebItem(dataItem, idSleb, dataSleb);

	sumSleb($(this).closest('tr').prev());
	sumSlebItem(parent);
	totalAmount();
})
